package in.project.coaching;

import androidx.appcompat.app.AppCompatActivity;
import in.project.coaching.Model.LoginModel;
import in.project.coaching.Model.RegistrationModel;
import in.project.coaching.Retrofit.ApiClient;
import in.project.coaching.Retrofit.ApiInterface;
import in.project.coaching.Utils.SharedPreferenceUtility;
import in.project.coaching.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static android.widget.Toast.LENGTH_LONG;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = RegistrationActivity.class.getSimpleName();
    Button registerBtn;
    EditText name;
    EditText mobileNo;
    EditText password;
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        baseActivity = this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        registerBtn = findViewById(R.id.registerBtn);
        name = findViewById(R.id.nameTxt);
        password= findViewById(R.id.passwordTxt);
        mobileNo = findViewById(R.id.mobileTxt);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = name.getText().toString().trim();
                String passwrd = password.getText().toString().trim();
                String mobile = mobileNo.getText().toString().trim();

                if (userName.length() == 0)
                {
                    showToastAtCentre("Please enter your Name", LENGTH_LONG);
                    return;
                }

                if (mobile.length() == 0)
                {
                    showToastAtCentre("Please enter your Email/ Mobile number", LENGTH_LONG);
                    return;
                }
                if (passwrd.length() == 0)
                {
                    showToastAtCentre("Please enter the Password", LENGTH_LONG);
                    return;
                }
                register_user(userName,mobile,passwrd);
            }
        });
    }

    public void register_user(final String usrName, final String email, final String passwrd)
    {
        Utils.showBusyAnimation(baseActivity,"Registering..");

        Call<RegistrationModel> call = apiService.registerUser(usrName,email,passwrd);

        call.enqueue(new Callback<RegistrationModel>() {
            @Override
            public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getStatus())
                    {
                        Intent intent = new Intent(RegistrationActivity.this,DashboardActivity.class);
                        SharedPreferenceUtility.saveUserToken(getApplicationContext(),usrName,response.body().getToken(), response.body().getData().getId());
                        startActivity(intent);
                        finish();
                        Intent filter = new Intent();
                        filter.setAction("finish_activity");
                        sendBroadcast(filter);
                        Log.d(RegistrationActivity.class.getSimpleName(),"Broadcast Sent!!!!");

                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }
                else {
                    try {
                        JSONObject obj = new JSONObject(response.errorBody().string());
                        showToastAtCentre(obj.getString("message"),LENGTH_LONG);
                        Log.d(TAG, obj.getString("message"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<RegistrationModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }
}
