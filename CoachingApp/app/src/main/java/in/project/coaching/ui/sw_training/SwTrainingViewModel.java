package in.project.coaching.ui.sw_training;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SwTrainingViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SwTrainingViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue("This is tools fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}