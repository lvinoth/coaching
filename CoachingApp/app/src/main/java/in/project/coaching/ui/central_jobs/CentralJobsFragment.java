package in.project.coaching.ui.central_jobs;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import in.project.coaching.Adapters.SubCategoryAdapter;
import in.project.coaching.DashboardActivity;
import in.project.coaching.LoginActivity;
import in.project.coaching.Model.LoginModel;
import in.project.coaching.Model.SubCategoryModel;
import in.project.coaching.R;
import in.project.coaching.Retrofit.ApiClient;
import in.project.coaching.Retrofit.ApiInterface;
import in.project.coaching.Utils.SharedPreferenceUtility;
import in.project.coaching.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class CentralJobsFragment extends Fragment {

    private static final String TAG = CentralJobsFragment.class.getSimpleName();
    private CentralJobsViewModel centralJobsViewModel;
    private ApiInterface apiService;
    private Activity baseActivity;
    private Toast mToast;
    private List<SubCategoryModel.Data> subcategoryList = new ArrayList<>();
    private RecyclerView subCategoryView;
    private SubCategoryAdapter subCategoryAdapter;

    public static CentralJobsFragment newInstance() {
        return new CentralJobsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.central_jobs_fragment, container, false);
        baseActivity = getActivity();
        apiService = ApiClient.getClient().create(ApiInterface.class);
        subCategoryView = root.findViewById(R.id.subCategoryView);
        centralJobsViewModel = ViewModelProviders.of(this).get(CentralJobsViewModel.class);
        centralJobsViewModel.getSubcategoryData().observe(this, new Observer<List<SubCategoryModel.Data>>() {
            @Override
            public void onChanged(@Nullable List<SubCategoryModel.Data> dataList) {
                subCategoryAdapter = new SubCategoryAdapter(getActivity(), dataList);
                subCategoryView.setLayoutManager(new LinearLayoutManager(baseActivity));
                subCategoryView.setAdapter(subCategoryAdapter);
            }
        });
        getSubCategory(1);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO: Use the ViewModel
    }

    public void getSubCategory(final int categoryId)
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");
        String token = SharedPreferenceUtility.getUserToken(baseActivity);
        Call<SubCategoryModel> call = apiService.getSubcategory("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJhZjUwYWFlNjQ1MDQwZmExOTE4YTIyMmU3NDYwOWYzYmIxNzFjMzYxNWNjN2ZhMzQ4MDYzZWU5MmJlNTgyYjk3NDcxZGQxNDE0NGVlODk1In0.eyJhdWQiOiIxIiwianRpIjoiYmFmNTBhYWU2NDUwNDBmYTE5MThhMjIyZTc0NjA5ZjNiYjE3MWMzNjE1Y2M3ZmEzNDgwNjNlZTkyYmU1ODJiOTc0NzFkZDE0MTQ0ZWU4OTUiLCJpYXQiOjE1ODc5MDk5MDIsIm5iZiI6MTU4NzkwOTkwMiwiZXhwIjoxNjE5NDQ1OTAyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.KOkNTHlc0pYZIK7GxFNUsV1wcIDsiAo3rWEqdzIafYqcj8tXrM8SIFlMJmEuy3JjZ2bdEw22GxMweST9_moibDo5hJn4COSJOyAt1u-OhR8h_A2KK9fpzPe0b-1Vb0KACljwJfa-Wk1Ld9zVAIJ8Q9tDx1DauPA-Y3ud7KN7M8EQenSyX3y_RUp-gkQa5jOiPt-UsJp5RjVpenDjx_1G0iXtNx4aCNdvW1tXRfAizFdD7_Jhn1WgEf5HDXWJ5_8o612C4dX2--TAYflHd6ACz9ZFL-sTpQsSxGFowXC3jHmBeMKYZW3jIOAnbFKssksQy3o2AMQ-wywtK3yIPAmWqzLXeTrH2ulC0gOyM-t7C1ldnVks8E3YGBb1uOebk3Gks6Pv-YJf1oN0lDefTVf_WJtW6G_5qIpieYmXmMUAjeAUTiPmKKyAqoCzGErxwP7i4V1S516tnN2sD7A9Ks4Dvke7EBd765XrDvHhVaiNvJOxzXA1K09vZVSBG63aZ-QLFRf1lUR4wAZgCJ9OHVBzIbD6uWm4gdoTB6FTMc6pT3IXo4y65kaENe895r-25tbaTCOTLZM9n732dEBU4vnHaj88yPhHU0S76HCWvaQJeSvEenJuhIUtLqF0ixURat4k77dMmGfQj80lytJfodrbhPPR8l4a1bLYJBSgkLkWW4A");

        call.enqueue(new Callback<SubCategoryModel>() {
            @Override
            public void onResponse(Call<SubCategoryModel> call, Response<SubCategoryModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getStatus())
                    {
                        if (response.body().getData() != null) {
                            subcategoryList = response.body().getData();
                            centralJobsViewModel.setSubcategoryData(subcategoryList);
                        }
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }
                else {
                    try {
                        JSONObject obj = new JSONObject(response.errorBody().string());
                        showToastAtCentre(obj.getString("message"),LENGTH_LONG);
                        Log.d(TAG, obj.getString("message"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<SubCategoryModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(baseActivity, "Something went wrong!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(baseActivity, message, duration);
        mToast.show();
    }

}
