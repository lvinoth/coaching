package in.project.coaching.ui.ips_academies;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class IpsAcademiesViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public IpsAcademiesViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Food Categories");
    }

    public LiveData<String> getText() {
        return mText;
    }
}