package in.project.coaching.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import in.project.coaching.R;

public class SharedPreferenceUtility {

    private static SharedPreferences sharedPreferences;


    public static SharedPreferences getSharedPreferenceInstance(Context context) {
        if (sharedPreferences == null)
            sharedPreferences = context.getSharedPreferences(
                    context.getString(R.string.USER_PREFERENCES),
                    Context.MODE_PRIVATE);

        return sharedPreferences;
    }

    //* SAVING USER ID & TOKEN *//

    public static void saveUserToken(Context context, String mailOrPhone, String token, int userId) {
        SharedPreferences.Editor sharedPreferencesEditor = getSharedPreferenceInstance(context).edit();

        sharedPreferencesEditor.putString("usr_mobile", mailOrPhone);
        sharedPreferencesEditor.putString("usr_token", token);
        sharedPreferencesEditor.putInt("usr_id", userId);
        sharedPreferencesEditor.commit();
    }


    public static int getUserId(Context context) {
        return getSharedPreferenceInstance(context).getInt("usr_id", -1);
    }

    public static String getUserNameOrMobile(Context context) {
        return getSharedPreferenceInstance(context).getString("usr_mobile", "");
    }

    public static String getUserToken(Context context) {
        return getSharedPreferenceInstance(context).getString("usr_token", "");
    }

}
