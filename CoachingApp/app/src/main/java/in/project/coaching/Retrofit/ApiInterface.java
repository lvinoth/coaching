package in.project.coaching.Retrofit;

import in.project.coaching.Model.LoginModel;
import in.project.coaching.Model.RegistrationModel;
import in.project.coaching.Model.SubCategoryModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("register")
    Call<RegistrationModel> registerUser(@Field("name") String name, @Field("email") String mobileNoOrEmail,
                                         @Field("password") String password);

    @FormUrlEncoded
    @POST("login")
    Call<LoginModel> authenticateLogin(@Field("username") String username, @Field("password") String password);

    @Headers({ "Content-Type: application/json;charset=UTF-8"})
    @GET("subcategories/1")
    Call<SubCategoryModel> getSubcategory(@Header("Authorization") String authHeader);

}
