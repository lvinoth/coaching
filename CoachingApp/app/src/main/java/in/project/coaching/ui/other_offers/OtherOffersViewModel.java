package in.project.coaching.ui.other_offers;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import in.project.coaching.Adapters.ReportAdapter;
import in.project.coaching.ReportModel;

public class OtherOffersViewModel extends ViewModel {

    private Context mContext;
    private MutableLiveData<String> mText;
    private MutableLiveData<ReportAdapter> adapter;
    private List<ReportModel> reportList;

    public OtherOffersViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue("This is share fragment");
        adapter = new MutableLiveData<>();

        ReportModel reportModel;
        reportList = new ArrayList<>();
        for (int i=0; i< 10; i++)
        {
            reportModel = new ReportModel();
            reportList.add(reportModel);
        }
    }

    public LiveData<String> getText() {
        return mText;
    }

    public void setmContext(Context contxt)
    {
        this.mContext = contxt;
        ReportAdapter adap = new ReportAdapter(mContext,reportList);
        adapter.setValue(adap);
    }

    public LiveData<ReportAdapter> getAdapter() {
        return adapter;
    }
}