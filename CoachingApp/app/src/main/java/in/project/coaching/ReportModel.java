package in.project.coaching;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class ReportModel {


        private String reportNo;
        private String date;
        private int totalEarnings;
        private int totalOrders;
        private int totalDining;
        private int totalTakeAway;


    public String getReportNo() {
        return reportNo;
    }

    public void setReportNo(String reportNo) {
        this.reportNo = reportNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTotalEarnings() {
        return totalEarnings;
    }

    public void setTotalEarnings(int totalEarnings) {
        this.totalEarnings = totalEarnings;
    }

    public int getTotalOrders() {
        return totalOrders;
    }

    public void setTotalOrders(int totalOrders) {
        this.totalOrders = totalOrders;
    }

    public int getTotalDining() {
        return totalDining;
    }

    public void setTotalDining(int totalDining) {
        this.totalDining = totalDining;
    }

    public int getTotalTakeAway() {
        return totalTakeAway;
    }

    public void setTotalTakeAway(int totalTakeAway) {
        this.totalTakeAway = totalTakeAway;
    }
}
