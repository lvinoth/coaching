package in.project.coaching;

public class FoodItems {

    private  String foodID;
    private String foodName;
    private int foodImage;
    private String foodPrice;
    private String foodDiscountPrice;
    private int cartQty;

    public String getFoodID() {
        return foodID;
    }

    public void setFoodID(String foodID) {
        this.foodID = foodID;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getFoodImage() {
        return foodImage;
    }

    public void setFoodImage(int foodImage) {
        this.foodImage = foodImage;
    }

    public String getFoodPrice() {
        return foodPrice;
    }

    public void setFoodPrice(String foodPrice) {
        this.foodPrice = foodPrice;
    }

    public String getFoodDiscountPrice() {
        return foodDiscountPrice;
    }

    public void setFoodDiscountPrice(String foodDiscountPrice) {
        this.foodDiscountPrice = foodDiscountPrice;
    }

    public int getCartQty() {
        return cartQty;
    }

    public void setCartQty(int cartQty) {
        this.cartQty = cartQty;
    }
}
