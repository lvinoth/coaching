package in.project.coaching;

import androidx.appcompat.app.AppCompatActivity;
import in.project.coaching.Utils.SharedPreferenceUtility;
import in.project.coaching.Utils.Utils;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {
    private Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.makeStatusBarTransparent(getWindow(), this);
        setContentView(R.layout.activity_splash);
        showMainScreen();
    }
    private void showMainScreen() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Launch Main Activity here

                if (SharedPreferenceUtility.getUserId(getApplicationContext()) == -1) {
                    Intent i = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(i);
                } else {
                    Intent i = new Intent(getBaseContext(), DashboardActivity.class);
                    startActivity(i);
                }

                finish();
            }
        }, 2000);
    }
}
