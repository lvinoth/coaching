package in.project.coaching.ui.state_jobs;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import in.project.coaching.Adapters.OrderAdapter;
import in.project.coaching.OrderModel;
import in.project.coaching.R;

public class StateJobsViewModel extends ViewModel {

    private Context mContext;
    String[] orderIDs = new String[]{"#4569877", "#4569878","#4569879"};
    String[] statuses = new String[]{"Pending", "Billed","Delivered"};
    int[] colors = new int[]{R.drawable.red_bg, R.drawable.green_bg, R.drawable.orange_bg};
    private MutableLiveData<String> mText;
    private MutableLiveData<OrderAdapter> adapter;
    private List<OrderModel> ordersList;

    public StateJobsViewModel() {
        mText = new MutableLiveData<>();
        adapter = new MutableLiveData<>();
//        mText.setValue("This is send fragment");

        OrderModel order;
        ordersList = new ArrayList<>();
        for (int i=0; i< 3; i++)
        {
            order = new OrderModel();
            order.setOrderId(orderIDs[i]);
            order.setStatus(statuses[i]);
            order.setColor(colors[i]);
            ordersList.add(order);
        }
    }

    public LiveData<String> getText() {
        return mText;
    }

    public void setmContext(Context contxt)
    {
        this.mContext = contxt;
        OrderAdapter adap = new OrderAdapter(mContext,ordersList);
        adapter.setValue(adap);
    }


    public LiveData<OrderAdapter> getAdapter() {
        return adapter;
    }
}