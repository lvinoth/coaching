package in.project.coaching.ui.ips_academies;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import in.project.coaching.R;

public class IpsAcademiesFragment extends Fragment {

    private IpsAcademiesViewModel ipsAcademiesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ipsAcademiesViewModel =
                ViewModelProviders.of(this).get(IpsAcademiesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_ips_academies, container, false);
        final TextView textView = root.findViewById(R.id.text_slideshow);
        ipsAcademiesViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}