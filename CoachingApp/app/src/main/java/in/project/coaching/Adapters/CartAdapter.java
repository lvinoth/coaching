package in.project.coaching.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import in.project.coaching.CartItems;
import in.project.coaching.DashboardActivity;
import in.project.coaching.R;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder>  {

    private List<CartItems> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private Activity baseActivity;
    

    // data is passed into the constructor
    public CartAdapter(Context context, List<CartItems> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.baseActivity = (DashboardActivity)context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.cart_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final CartItems food = mData.get(position);
        holder.food_name.setText(food.getFoodCartName());
        holder.foodCartQuantity.setText(String.valueOf(food.getCartQty()));


        holder.decreaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.increaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView food_name;
        TextView foodCartQuantity;
        TextView increaseCartQuantity;
        TextView decreaseCartQuantity;

        ViewHolder(View itemView) {
            super(itemView);
            food_name = itemView.findViewById(R.id.cartItemName);
            foodCartQuantity = itemView.findViewById(R.id.foodCartQuantityCart);
            decreaseCartQuantity = itemView.findViewById(R.id.decrementOrderCart);
            increaseCartQuantity = itemView.findViewById(R.id.incrementOrderCart);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // convenience method for getting data at click position
    CartItems getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
