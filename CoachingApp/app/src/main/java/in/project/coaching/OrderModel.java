package in.project.coaching;

import java.util.List;

/**
 * Created by shastamobiledev on 03/04/18.
 */

public class OrderModel {


        private String orderId;
        private String status;
        private int color;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
