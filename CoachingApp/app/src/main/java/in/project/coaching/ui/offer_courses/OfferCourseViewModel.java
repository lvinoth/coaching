package in.project.coaching.ui.offer_courses;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

import in.project.coaching.Adapters.FoodAdapter;
import in.project.coaching.FoodItems;

public class OfferCourseViewModel extends ViewModel {

    private Context mContext;
    String[] foods = new String[]{"Fish Curry", "Fish Fry", "Chicken Fry", "Kadai Chicken", "Tandoori Chicken", "Grill Chicken", "Mutton Fry", "Mutton Kebab", "Prawn Gravy","Mushroom Fry"};
    int[] foodImages = new int[]{};
    private MutableLiveData<String> mText;
    private MutableLiveData<FoodAdapter> adapter;
    private List<FoodItems> foodItemsList;
    public OfferCourseViewModel() {
        mText = new MutableLiveData<>();
        adapter = new MutableLiveData<>();
//        mText.setValue("This is gallery fragment");

        FoodItems items;
        foodItemsList = new ArrayList<>();
        for (int i=0; i< 10; i++)
        {
            items = new FoodItems();
            items.setFoodID(String.valueOf(i));
            items.setFoodName(foods[i]);
            items.setFoodImage(foodImages[i]);
            items.setFoodPrice("₹ 150");
            items.setFoodDiscountPrice("-15%");
            items.setCartQty(i == 1 ? 3 : 0);
            foodItemsList.add(items);
        }

    }

    public LiveData<String> getText() {
        return mText;
    }

    public void setmContext(Context contxt)
    {
        this.mContext = contxt;
        FoodAdapter adap = new FoodAdapter(mContext,foodItemsList);
        adapter.setValue(adap);
    }


    public LiveData<FoodAdapter> getAdapter() {
        return adapter;
    }

}