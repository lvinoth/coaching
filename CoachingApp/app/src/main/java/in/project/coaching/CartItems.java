package in.project.coaching;

public class CartItems {

    private  String foodID;
    private String foodCartName;
    private int cartQty;

    public String getFoodID() {
        return foodID;
    }

    public void setFoodID(String foodID) {
        this.foodID = foodID;
    }

    public String getFoodCartName() {
        return foodCartName;
    }

    public void setFoodCartName(String foodCartName) {
        this.foodCartName = foodCartName;
    }

    public int getCartQty() {
        return cartQty;
    }

    public void setCartQty(int cartQty) {
        this.cartQty = cartQty;
    }
}
