package in.project.coaching;

import androidx.appcompat.app.AppCompatActivity;
import in.project.coaching.Model.LoginModel;
import in.project.coaching.Retrofit.ApiClient;
import in.project.coaching.Retrofit.ApiInterface;
import in.project.coaching.Utils.SharedPreferenceUtility;
import in.project.coaching.Utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static android.widget.Toast.LENGTH_LONG;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    Button loginBtn;
    TextView registerTxt;
    private Activity baseActivity;
    private Toast mToast;
    private ApiInterface apiService;
    EditText usernameTxt;
    EditText passwordTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.makeStatusBarTransparent(getWindow(),this);
        setContentView(R.layout.activity_login);
        baseActivity = this;
        apiService = ApiClient.getClient().create(ApiInterface.class);
        usernameTxt = findViewById(R.id.userNameTxt);
        passwordTxt = findViewById(R.id.passwordTxt);
        loginBtn = findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = usernameTxt.getText().toString();
                String password = passwordTxt.getText().toString();

                if (userName.length() == 0)
                {
                    showToastAtCentre("Please enter your Email/ Mobile number", LENGTH_LONG);
                    return;
                }
                if (password.length() == 0)
                {
                    showToastAtCentre("Please enter the Password", LENGTH_LONG);
                    return;
                }

                authenticate_user(userName, password);
            }
        });
        registerTxt = findViewById(R.id.registerTxt);
        registerTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(intent);
            }
        });
        registerReceiver(broadcastReceiver,new IntentFilter("finish_activity"));
    }

    public void authenticate_user(final String usrName, final String passwrd)
    {
        Utils.showBusyAnimation(baseActivity,"Authenticating..");

        Call<LoginModel> call = apiService.authenticateLogin(usrName,passwrd);

        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {

                Utils.hideBusyAnimation(baseActivity);
                if(response.body() != null) {
                    if (response.body().getStatus())
                    {
                        Intent intent = new Intent(LoginActivity.this,DashboardActivity.class);
                        // Save the token in prefs
                        SharedPreferenceUtility.saveUserToken(getApplicationContext(),usrName,response.body().getToken(), response.body().getData().getId());
                        startActivity(intent);
                        finish();
                    }
                    else // Handle failure cases here
                    {
                        String mess = response.body().getMessage();
                        Log.d(TAG, "Response: " + mess);
                        showToastAtCentre("" + mess, LENGTH_LONG);
                    }
                }
                else {
                    try {
                        JSONObject obj = new JSONObject(response.errorBody().string());
                        showToastAtCentre(obj.getString("message"),LENGTH_LONG);
                        Log.d(TAG, obj.getString("message"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                Utils.hideBusyAnimation(baseActivity);
                Log.d(TAG, "Response: " + t.getMessage());
                if (t instanceof IOException) {
                    Toast.makeText(baseActivity, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context arg0, Intent intent) {
            String action = intent.getAction();
            if (action.equals("finish_activity")) {

                Log.d(LoginActivity.class.getSimpleName(),"Broadcast Received!!!!");
                finish();
            }
        }
    };

    private void showToastAtCentre(String message, int duration) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(getApplicationContext(), message, duration);
        mToast.show();
    }
}
