package in.project.coaching.ui.state_jobs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.project.coaching.Adapters.OrderAdapter;
import in.project.coaching.R;

public class StateJobsFragment extends Fragment {

    private StateJobsViewModel stateJobsViewModel;
    private RecyclerView orderRecyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        stateJobsViewModel =
                ViewModelProviders.of(this).get(StateJobsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_state_jobs, container, false);
        orderRecyclerView = root.findViewById(R.id.ordersRecyclerView);
        orderRecyclerView.setClipToOutline(true);
        stateJobsViewModel.setmContext(getActivity());
        final TextView textView = root.findViewById(R.id.text_send);
        stateJobsViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        stateJobsViewModel.getAdapter().observe(this, new Observer<OrderAdapter>() {
            @Override
            public void onChanged(OrderAdapter orderAdapter) {
                orderRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
                orderRecyclerView.setAdapter(orderAdapter);
            }
        });
        return root;
    }
}