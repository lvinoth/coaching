package in.project.coaching.ui.central_jobs;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import in.project.coaching.Model.SubCategoryModel;

public class CentralJobsViewModel extends ViewModel {
    // TODO: Implement the ViewModel

    private MutableLiveData<String> mText;
    private MutableLiveData<List<SubCategoryModel.Data>> subcategoryData;

    public CentralJobsViewModel() {
        mText = new MutableLiveData<>();
        subcategoryData = new MutableLiveData<>();
    }

    public void setSubcategoryData(List<SubCategoryModel.Data> subcategoryData) {
        this.subcategoryData.setValue(subcategoryData);
    }

    public LiveData<String> getText() {
        return mText;
    }
    public LiveData<List<SubCategoryModel.Data>> getSubcategoryData() {
        return this.subcategoryData;
    }
}
