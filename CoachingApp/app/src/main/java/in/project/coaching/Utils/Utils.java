package in.project.coaching.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import in.project.coaching.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class Utils {

    public static void makeStatusBarTransparent(Window window, Context context)
    {
        ((Activity)context).requestWindowFeature(Window.FEATURE_NO_TITLE);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

    }

    private static Dialog progressDialog;

    public static void showBusyAnimation(final Activity baseActivity, final String progressText) {

        baseActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                //Log.d(UtilsClass.class.getSimpleName(),"showBusyAnimation");

                if (progressDialog == null){
                    createLoader(baseActivity);
                    if(!progressDialog.isShowing()) {
                        progressDialog.show();
                    }
                }
                else {
                    if(progressDialog != null && !progressDialog.isShowing()) {
                        progressDialog.show();
                    }
                }
            }
        });
    }

    public static void createLoader(Context context)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View progressLayout = inflater.inflate(R.layout.progressdialog, null);

        TextView loadingText = progressLayout.findViewById(R.id.tv_loadingmsg);

        loadingText.setText("Loading..");

        progressDialog = new Dialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setContentView(progressLayout);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
    }



    public static void hideBusyAnimation(final Activity baseActivity) {

        baseActivity.runOnUiThread(new Runnable() {
            public void run() {
                //Log.d(UtilsClass.class.getSimpleName(),"hideBusyAnimation");

                try {
                    if ((progressDialog != null) && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                } catch (final Exception e) {
                    e.printStackTrace();
                } finally {
                    progressDialog = null;
                }
            }
        });

    }
}
