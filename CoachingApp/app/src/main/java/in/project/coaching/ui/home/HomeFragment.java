package in.project.coaching.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import in.project.coaching.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private LinearLayout foodItemLayout;
    private LinearLayout orderLayout;
    private LinearLayout profileLayout;
    private LinearLayout reportLayout;
    private LinearLayout categoryLayout;
    private NavController navController;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
//        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
//                textView.setText(s);
            }
        });

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        foodItemLayout = root.findViewById(R.id.foodItemView);
        foodItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_offer_course);
            }
        });

        orderLayout = root.findViewById(R.id.central_gov);
        orderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_central_govt_jobs);
            }
        });

        profileLayout = root.findViewById(R.id.profileView);
        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_sw_training);
            }
        });

        reportLayout= root.findViewById(R.id.reportView);
        reportLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_other_offers);
            }
        });

        categoryLayout= root.findViewById(R.id.categoryView);
        categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.nav_ips_academies);
            }
        });

        return root;
    }
}