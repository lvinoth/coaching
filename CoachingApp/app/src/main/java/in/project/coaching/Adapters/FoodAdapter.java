package in.project.coaching.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import in.project.coaching.DashboardActivity;
import in.project.coaching.FoodItems;
import in.project.coaching.R;

public class FoodAdapter extends RecyclerView.Adapter<FoodAdapter.ViewHolder>  {

    private List<FoodItems> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;
    private Activity baseActivity;



    // data is passed into the constructor
    public FoodAdapter(Context context, List<FoodItems> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.baseActivity = (DashboardActivity)context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public FoodAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.food_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final FoodItems food = mData.get(position);
        holder.food_name.setText(food.getFoodName());
        holder.food_price.setText(food.getFoodPrice());


        if(position == 1)
        {
            holder.addCartLayout.setVisibility(View.GONE);
            holder.addMoreToCartLayout.setVisibility(View.VISIBLE);
        }
        else {
            holder.addCartLayout.setVisibility(View.VISIBLE);
            holder.addMoreToCartLayout.setVisibility(View.GONE);
        }

        holder.food_imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        holder.food_imageView.setClipToOutline(true);
        holder.food_imageView.setBackground(mContext.getDrawable(food.getFoodImage()));

//        Picasso.with(mContext).load(R.drawable.food_ph)
//                .placeholder(R.drawable.food_ph)
//                .error(R.drawable.food_ph)
//                .into(holder.food_imageView);

        holder.foodCartQuantity.setText(String.valueOf(food.getCartQty()));

        holder.addIngredients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.decreaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.increaseCartQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView food_imageView;
        TextView food_name;
        TextView food_price;
        Button addIngredients;
        LinearLayout addCartLayout;
        LinearLayout addMoreToCartLayout;
        TextView foodCartQuantity;
        TextView increaseCartQuantity;
        TextView decreaseCartQuantity;

        ViewHolder(View itemView) {
            super(itemView);
            food_imageView = itemView.findViewById(R.id.food_imageView);
            food_name = itemView.findViewById(R.id.food_nameView);
            food_price = itemView.findViewById(R.id.food_costView);
            addIngredients = itemView.findViewById(R.id.addCartBtn);
            addCartLayout = itemView.findViewById(R.id.addCartLayout);
            addMoreToCartLayout = itemView.findViewById(R.id.addMoreToCartLayout);
            foodCartQuantity = itemView.findViewById(R.id.foodCartQuantity);
            decreaseCartQuantity = itemView.findViewById(R.id.decrementOrder);
            increaseCartQuantity = itemView.findViewById(R.id.incrementOrder);
            food_imageView.setClipToOutline(true);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // convenience method for getting data at click position
    FoodItems getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
