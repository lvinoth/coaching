package in.project.coaching.ui.offer_courses;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import in.project.coaching.Adapters.FoodAdapter;
import in.project.coaching.R;

public class OfferCourseFragment extends Fragment {

    private OfferCourseViewModel offerCourseViewModel;
    private RecyclerView foodRecyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        offerCourseViewModel =
                ViewModelProviders.of(this).get(OfferCourseViewModel.class);
        View root = inflater.inflate(R.layout.fragment_offer_courses, container, false);
        final TextView textView = root.findViewById(R.id.text_gallery);
        foodRecyclerView = root.findViewById(R.id.foodItemsRecyclerView);
        offerCourseViewModel.setmContext(getActivity());
        offerCourseViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        offerCourseViewModel.getAdapter().observe(this, new Observer<FoodAdapter>() {
            @Override
            public void onChanged(FoodAdapter foodAdapter) {
                foodRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                foodRecyclerView.setAdapter(foodAdapter);
            }
        });
        return root;
    }
}