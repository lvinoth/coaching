package in.project.coaching.ui.other_offers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.project.coaching.Adapters.ReportAdapter;
import in.project.coaching.R;

public class OtherOffersFragment extends Fragment {

    private OtherOffersViewModel otherOffersViewModel;
    private RecyclerView reportRecyclerView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        otherOffersViewModel =
                ViewModelProviders.of(this).get(OtherOffersViewModel.class);
        View root = inflater.inflate(R.layout.fragment_other_offers, container, false);
        final TextView textView = root.findViewById(R.id.text_share);

        reportRecyclerView = root.findViewById(R.id.reportRecyclerView);
        otherOffersViewModel.setmContext(getActivity());

        otherOffersViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        otherOffersViewModel.getAdapter().observe(this, new Observer<ReportAdapter>() {
            @Override
            public void onChanged(ReportAdapter orderAdapter) {
                reportRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));
                reportRecyclerView.setAdapter(orderAdapter);
            }
        });

        return root;
    }
}