package in.project.coaching.ui.sw_training;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import in.project.coaching.R;

public class SwTrainingFragment extends Fragment {

    private SwTrainingViewModel swTrainingViewModel;
    private ImageView profilePicView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        swTrainingViewModel =
                ViewModelProviders.of(this).get(SwTrainingViewModel.class);
        View root = inflater.inflate(R.layout.fragment_sw_training, container, false);
        final TextView textView = root.findViewById(R.id.text_tools);
        profilePicView = root.findViewById(R.id.profilePicView);
        swTrainingViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        profilePicView.setClipToOutline(true);
        return root;
    }
}